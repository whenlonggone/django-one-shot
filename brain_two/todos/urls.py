from django.urls import path
from todos.views import (
    show_TodoList,
    show_TodoItem,
    create_new_list,
    update_list_name,
    delete_list,
    create_item,
    update_item,
)

urlpatterns = [
    path("", show_TodoList, name="todo_list_list"),
    path("<int:id>/", show_TodoItem, name="todo_list_detail"),
    path("create/", create_new_list, name="todo_list_create"),
    path("<int:id>/edit/", update_list_name, name="todo_list_update"),
    path("<int:id>/delete/", delete_list, name="todo_list_delete"),
    path("items/create/", create_item, name="todo_item_create"),
    path("items/<int:id>/edit/", update_item, name="todo_item_update"),
]
